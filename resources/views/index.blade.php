<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/css/app.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <style>
    @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');
  </style>
  <title>Reviews</title>
  <meta name="description" content="Using AI and a team of experts, HyperComply automates security reviews and accelerates due diligence, so you can onboard new tools and partners with confidence. Close deals faster with our solution for sales teams and simplify vendor reviews for procurement teams. Trusted by top teams in the industry.">
</head>
<body>
<header class="header">
  <div class="container">
    <div class="navbar">
      <div class="row">
        <div class="col-md-12">
          <nav class="menu_nav">
            <a href="#" class="link">Product</a>
            <a href="#" class="link">Pricing</a>
            <a href="#" class="link">Company</a>
            <a href="#" class="link">Learn More</a> 
            
          <div class="header-buttons">
            <a href="#" class="login">Sign In</a>
            <button class="button">Get Started</button>
          </div>
          </nav>
        </div>
       
      </div>
    </div>
  </div>
</header>

<div class="hero">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="title-wrapper">
          <h1 class="title">Securiry <br>reviews,<br> solved</h1>
          <div class="hero-description">
            <span>Using AI and a team of experts, HyperComply automates security reviews and accelerates due diligence, so you can onboard new tools and partners with confidence.</span>
          </div>
          <button class="button">Get a Demo</button>
        </div>
      </div>
      <div class="col-md-6">
        <div class="hero-images">
          <img src="images/IMAGE.png" alt="Image">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="aside">
          <div class="aside-text">
            <p>We've saved 10,000+ hours for the smartest businesses in the world.</p>
          </div>
          <figure class="aside-images">
            <img src="images/aside_image1.png" alt="Image">
            <img src="images/aside_image2.png" alt="Image">
            <img src="images/aside_image3.png" alt="Image">
            <img src="images/aside_image4.png" alt="Image">
            <img src="images/aside_image5.png" alt="Image">
            <img src="images/aside_image6.png" alt="Image">
          </figure>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="list-content">
  <div class="container">
    <div class="row">
      <div class="col-md-12 subtitle">For Sales teams at startup companies</div>
    </div>
    <div class="row">
      <div class="col-md-12 title">Close deals faster</div>
    </div>
    <div class="row">
      <div class="col-md-12 text">
      Don't let security reviews slow you down. We make sure you respond to security questionnaires in 1 day, guaranteed.</div>
    </div>
    <div class="row">
      <div class="col-md-12 button-list">
        <button class="button">Learn More</button>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 list">
        <div class="row items">
          <div class="col-md-2 list-item">Automation + human review</div>
          <div class="col-md-2 list-item">Always learning</div>
        <div class="col-md-2 list-item">Simple and flexible</div>
      </div>
      <div class="row">
        <div class="col-md-12 image-items">
          <img src="images/proccent.png" alt="Image">
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<div class="list-content">
  <div class="container">
  <div class="row">
    <div class="col-md-12 subtitle">For procurement teams who care about security</div>
  </div>
  <div class="row">
    <div class="col-md-12 title">Easy vendor reviews</div>
  </div>
  <div class="row">
    <div class="col-md-12 text">
      HyperComply makes it easy to send security questionnaires to new vendors and monitor your network of tools over time.
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 button-list">
      <button class="button">Learn More</button>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 list">
      <div class="row items">
        <div class="col-md-2 list-item">Find and fill security gaps</div>
        <div class="col-md-2 list-item">Streamline your<br> procurement processes</div>
        <div class="col-md-2 list-item">Never start from scratch</div>
      </div>
      <div class="row">
        <figure class="col-md-12 image-vector-items">
          <div class="vector-image">
            <img src="images/vector.svg" alt="Image">
          </div>
          <img src="images/IMAGE2.png" alt="Image">
          <img src="images/IMAGE1.png" alt="Image">
        </figure>
      </div>
    </div>
  </div>
</div>
</div>


<div class="carousel">
<div class="container">
    <div class="row">
      <div class="col-lg-12 title">
        Trusted by top teams
        <div class="row">
          <div class="col-lg-4">
            <div id="slider-one" class="slider">
              <p>"HyperComply allows my team to fly through security reviews, extending the expertise of our IT Security team into the TAM and SC teams on the sales side of the business."</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div id="slider-two" class="slider">
              <img src="images/smarsh.png" alt="Image">
              <p>“HyperComply has enabled us to complete questionnaires quickly and accurately, and has had a hugely positive effect on speeding up our sales cycle.”</p>
              <div class="info">
                <img class="info" src="images/foto.png" alt="Image">
                <p>Desiree R, Sr. Director of Information Security</p>
              </div>  
            </div>
          </div>
          <div class="col-lg-4">
            <div id="slider-three" class="slider">
              <p>"With HyperComply it’s just a quick scroll through the questions. It might be five or ten minutes just to review, and it’s actually done right."</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="arrow">
              <figure class="right-arrow">
                <img src="images/arrow_right.svg" alt="Vector">
              </figure>
              <figure class="left-arrow">
                <img src="images/arrow_left.svg" alt="Vector">
              </figure>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
</div>

<div class="subject">
  <div class="container">
      <div class="row">
        <div class="col-lg-12 subject-title">
          <div class="title">Everything you need to get security reviews done fast and done right</div>
          <button class="button">Get a Demo</button>
        </div>
      </div>
  </div>
</div>

<div class="footer">
  <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-6">
          <div class="column">
            <div class="vector2">
              <img src="images/vector2.svg" alt="Vector">
            </div>
          </div>
        </div>
      <div class="col-lg-3 col-md-6">
        <div class="column">
          <h2 class="column-title">Legal</h2>
          <div class="column-entry">Privacy Policy</div>
        </div>
      </div>
    <div class="col-lg-3 col-md-6">
        <div class="column">
          <h2 class="column-title">Contact</h2>
          <div class="column-entry">Email us</div>
          <div class="column-entry">Support</div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="column">
          <h2 class="column-title">About</h2>
          <div class="column-entry">Company</div>
          <figure class="social-icons">
            <img src="images/twitter.svg" alt="Twitter icon">
            <img src="images/instagram.svg" alt="Instagram icon">
          </figure>
         </div>
      </div>
    </div>
  </div>
</body>
</html> 
