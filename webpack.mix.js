const mix = requare("laravel-mix");

mix.js('resources/scss/app.scss', 'public/css')
    .sass('resources/js/app.js', 'public/js');
